from django.apps import AppConfig


class BiodataganConfig(AppConfig):
    name = 'biodatagan'
