from django.shortcuts import render

# Create your views here.

def view_home(request):
	return render(request, 'homepage.html')
	
def view_about(request):
	return render(request, 'aboutme.html')
	
def view_record(request):
	return render(request, 'trackrecord.html')
	
def view_interest(request):
	return render(request, 'interest.html')
	
def view_songcover(request):
	return render(request, 'songcover.html')
	
def view_register(request):
	return render(request, 'formpage.html')

