---- STORY REFLECTION I ----

Nama            : Ramawajdi Kanishka Anwar
NPM             : 1706043595
Kode Asdos      : -
Alamat GitLab   : https://gitlab.com/ramawajdika/TrueLatihanPPW
Alamat Heroku   : https://ppw-d-45rabbit.herokuapp.com/

 Untuk  Challenge pertama pada mata kuliah PPW ini, Saya berkesempatan untuk mencoba membuat web untuk pertama kalinya yang berisikan biodata diri, biodata teman di belakang, dan biodata teman di depan saya. Yang saya lakukan saat memulai mengerjakan story sebelum challengenya adalah membuat local repo baru, lalu menyiapkan virtual environment pada repo tersebut. Setelah itu, saya membuat project Django baru. Saya melakukan beberapa penyesuaian pada Settings.py di project, urls.py di project, dan urls.py di app. Saya memerlukan beberapa file dari repo PPW tahun lalu seperti requirements.txt, procfile, deployment.sh, dan .yml secara bertahap karena setelah lama error saat deployment, saya mengambil file tersebut satu demi satu sampai bisa di deploy.  Setelah itu, saya mengedit html dan views.py sesuai kebutuhan saya untuk menampilkan biodata singkat.

 Karena saya masih belum paham penggunaan file static untuk gambar dan CSS, saya mengatur style langsung di file HTML nya dan mengupload image ke catbox.moe .  Sebelum saya push, saya membuat app baru pada akun Heroku saya. Ketika saya buka repo git saya, saya ganti variables di setting CI/CD  Variables dan mengisi APIKEY, APPNAME, dan APP_HOST.  Lalu pada hari ini, saya menambahkan konten dengan biodata teman di depan dan di belakang saya. Dan juga membuat app baru untuk keperluan challenge. 
 
Hal yang saya pelajari dari challenge ini adalah bagaimana secara dasar melakukan deployment ke Heroku untuk website yang kita buat. Dengan apa yang saya alami sebelumnya, masih banyak hal yang harus saya pelajari mengenai hal ini. Terutama pada pen-setting an Django project from scratch, dan plugin-plugin yang diperlukan untuk memenuhi deployment. Saya juga harus memahami bagaimana penggunaan static file pada html yang baik dan juga pemahaman mengenai struktur .py yang ada di project Django secara mendetil agar bisa mengetahui “how does it works” dan bisa mengimplementasikannya untuk tugas-tugas atau keperluan selanjutnya.
